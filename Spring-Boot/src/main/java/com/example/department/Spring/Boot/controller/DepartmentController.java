package com.example.department.Spring.Boot.controller;

import com.example.department.Spring.Boot.model.Department;
import com.example.department.Spring.Boot.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("exam")
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Department> departments() {
        return departmentService.retrieveDepartments();
    }

    @PostMapping(value = "/add")
    public Department add (@RequestBody Department department) {
        return departmentService.addDepartment(department);
    }

    @DeleteMapping(value = "/deleteEmployee")
    public void deleteById(@RequestParam("id") int id) {
        departmentService.deleteDepartment(id);
    }

    @PutMapping("/update")
    public void updateDepartment(@RequestBody Department department) throws Exception {
        departmentService.updateDepartment(department);
    }
}