package com.example.department.Spring.Boot.service;

import com.example.department.Spring.Boot.model.Department;
import com.example.department.Spring.Boot.repo.DepartmentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentService {

    @Autowired
    private DepartmentRepo departmentRepo;

    public List<Department> retrieveDepartments(){
        List<Department> departments = departmentRepo.findAll();
        return departments;
    }

    public Department addDepartment(Department department){

        return departmentRepo.save(department);
    }
    public void deleteDepartment(int id){
        departmentRepo.deleteById(id);
    }
    public void updateDepartment(Department department){
            departmentRepo.save(department);
    }

}