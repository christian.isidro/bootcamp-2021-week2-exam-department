package com.example.department.Spring.Boot.service;

import com.example.department.Spring.Boot.model.Department;
import com.example.department.Spring.Boot.repo.DepartmentRepo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import static java.util.List.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class DepartmentServiceTest {

    @InjectMocks
    DepartmentService service;

    @Mock
    DepartmentRepo repository;

    List<Department> departmentList;
    Department dept;

    @BeforeEach
    public void init(){
        dept = new Department(1, "SEG", "Christian", "BGC", LocalDateTime.now(), LocalDateTime.now());
        departmentList =  List.of(
                new Department(1, "QA", "Kenneth", "BGC", LocalDateTime.now(), LocalDateTime.now()),
                new Department(2, "Maintenance", "Lance", "BGC", LocalDateTime.now(), LocalDateTime.now()),
                new Department(3, "HR", "Luchico", "BGC", LocalDateTime.now(), LocalDateTime.now()),
                new Department(4, "Accounting", "Sherwin", "BGC", LocalDateTime.now(), LocalDateTime.now())
        );

    }

    @Test
    @DisplayName("Should display all departments")
    void retrieveDepartments() {
        Mockito.when(repository.findAll()).thenReturn(departmentList);
        assertEquals(departmentList, service.retrieveDepartments());
    }

    @Test
    @DisplayName("Should add a new departments")
    void addDepartment() {
        Mockito.when(repository.save(dept)).thenReturn(dept);
        assertEquals(dept, service.addDepartment(dept));
    }
    @Test
    @DisplayName("Should delete an departments")
    void shouldDeleteDepartment() {
        service.deleteDepartment(dept.getId());
    }

    @Test
    @DisplayName("This should update a product")
    public void shouldUpdateProduct() {
        service.updateDepartment(dept);
    }




}